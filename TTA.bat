@echo off
set cfg_path=configs\electronic_eye\finetune_cascade_rcnn_res2net50_fpn_sb_gn_giou.yml
set weight_path=output\finetune_cascade_rcnn_res2net50_fpn_sb_gn_giou\model_final.pdparams

echo cfg path is : %cfg_path%
echo weight path is : %weight_path%

echo start TTA eval... 
for %%i in (0,1,2,3) do (
    echo TTA id: %%i & python tools/change_yml.py --sizeid %%i & python tools/eval.py -c %cfg_path% -o weights=%weight_path% & ren bbox.json bbox_%%i.json
)
python tools/soft_nms.py

pause