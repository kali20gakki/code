# Copyright (c) 2019 PaddlePaddle Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np

import paddle
from ppdet.core.workspace import register, serializable

__all__ = ['BalancedCIoULoss']


@register
@serializable
class BalancedCIoULoss(object):
    """
    Balanced L1 Loss, see https://arxiv.org/abs/1904.02701
    Args:
        alpha (float): hyper parameter of BalancedL1Loss, see more details in the paper
        gamma (float): hyper parameter of BalancedL1Loss, see more details in the paper
        beta  (float): hyper parameter of BalancedL1Loss, see more details in the paper
        loss_weights (float): loss weight
    """
    __inject__ = ['iou_loss']
    def __init__(self, alpha=0.5, gamma=1.5, beta=1.0, loss_weight=1.0, iou_loss = None):
        super(BalancedCIoULoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma
        self.beta = beta
        self.loss_weight = loss_weight
        self.iou_loss = iou_loss

    def __call__(
            self,
            x,
            y,
            inside_weight=None,
            outside_weight=None, ):

        #TODO add small weight
        
        alpha = self.alpha
        gamma = self.gamma
        beta = self.beta
        loss_weight = self.loss_weight

        reg_delta = self.bbox_transform(x)
        reg_target = self.bbox_transform(y)

        diff = self.iou_loss(reg_delta,reg_target)

        # diff = paddle.abs(x - y)
        b = np.e**(gamma / alpha) - 1
        less_beta = diff < beta
        ge_beta = diff >= beta
        less_beta = paddle.cast(x=less_beta, dtype='float32')
        ge_beta = paddle.cast(x=ge_beta, dtype='float32')
        less_beta.stop_gradient = True
        ge_beta.stop_gradient = True
        loss_1 = less_beta * (
            alpha / b * (b * diff + 1) * paddle.log(b * diff / beta + 1) -
            alpha * diff)
        loss_2 = ge_beta * (gamma * diff + gamma / b - alpha * beta)
        iou_weights = 1.0
        if inside_weight is not None and outside_weight is not None:
            iou_weights = inside_weight * outside_weight
        loss = (loss_1 + loss_2) * iou_weights
        loss = paddle.sum(loss, axis=-1) * loss_weight
        return loss
    
    def bbox_transform(self, deltas, weights=[0.1, 0.1, 0.2, 0.2]):
        wx, wy, ww, wh = weights

        deltas = paddle.reshape(deltas, shape=(0, -1, 4))

        dx = paddle.slice(deltas, axes=[2], starts=[0], ends=[1]) * wx
        dy = paddle.slice(deltas, axes=[2], starts=[1], ends=[2]) * wy
        dw = paddle.slice(deltas, axes=[2], starts=[2], ends=[3]) * ww
        dh = paddle.slice(deltas, axes=[2], starts=[3], ends=[4]) * wh

        dw = paddle.clip(dw, -1.e10, np.log(1000. / 16))
        dh = paddle.clip(dh, -1.e10, np.log(1000. / 16))

        pred_ctr_x = dx
        pred_ctr_y = dy
        pred_w = paddle.exp(dw)
        pred_h = paddle.exp(dh)

        x1 = pred_ctr_x - 0.5 * pred_w
        y1 = pred_ctr_y - 0.5 * pred_h
        x2 = pred_ctr_x + 0.5 * pred_w
        y2 = pred_ctr_y + 0.5 * pred_h

        x1 = paddle.reshape(x1, shape=(-1, ))
        y1 = paddle.reshape(y1, shape=(-1, ))
        x2 = paddle.reshape(x2, shape=(-1, ))
        y2 = paddle.reshape(y2, shape=(-1, ))

        return paddle.concat([x1, y1, x2, y2])