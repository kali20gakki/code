#!/bin/bash
cfg_file_name=cascade_rcnn_res2net50_fpn_sb_ciou_2x
echo 'start train...'
python -m paddle.distributed.launch --gpus 0,1,2,3 tools/train_v1.py -c configs/electronic_eye/$cfg_file_name.yml

echo 'start eval...'
python tools/eval.py -c configs/electronic_eye/$cfg_file_name.yml -o weights=output/$cfg_file_name/model_final.pdparams