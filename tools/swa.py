import os
import paddle

models_path = r'output\cascade_rcnn_res2net50_vd_ssld_gn_fpn_bl1_cos1x_stage1'

models = os.listdir(models_path)

model_pdparams = []
for m in models:
    if m.split('.')[-1] == 'pdparams':
        model_pdparams.append(m)

paths = []
for model in model_pdparams:
    paths.append(os.path.join(models_path, model))


models = [paddle.load(p) for p in paths]
model_num = len(models)
param_state_dict = models[-1]
new_state_dict = param_state_dict.copy()


for key in param_state_dict:
    print('key :', key)
    sum_weight = 0.0
    for m in models:
        sum_weight += m[key]
    avg_weight = sum_weight / model_num
    new_state_dict[key] = avg_weight

print('save swa model...')
save_path = os.path.join(models_path, "model_swa.pdparams")
paddle.save(new_state_dict, save_path)
